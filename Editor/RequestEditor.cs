﻿using UnityEngine;
using UnityEditor;

namespace Request
{
	public class RequestEditor : MonoBehaviour
	{
		private const string kQaMenuItem = "Kentaurus/Request To/QA";
		private const string kLiveMenuItem = "Kentaurus/Request To/Live";
		private const string kDevMenuItem = "Kentaurus/Request To/Dev";
		private const string kLocalMenuItem = "Kentaurus/Request To/Local";
		private const string kStageMenuItem = "Kentaurus/Request To/Stage";

		[MenuItem(kQaMenuItem)]
		private static void SwitchToQA()
		{
			RemoveAllCheckmark();
			SetTo(kQaMenuItem);
		}

		[MenuItem(kLiveMenuItem)]
		private static void SwitchToLive()
		{
			RemoveAllCheckmark();
			SetTo(kLiveMenuItem);
		}

		[MenuItem(kDevMenuItem)]
		private static void SwitchToDev()
		{
			RemoveAllCheckmark();
			SetTo(kDevMenuItem);
		}

		[MenuItem(kLocalMenuItem)]
		private static void SwitchToLocal()
		{
			RemoveAllCheckmark();
			SetTo(kLocalMenuItem);
		}

		[MenuItem(kStageMenuItem)]
		private static void SwitchToStage()
		{
			RemoveAllCheckmark();
			SetTo(kStageMenuItem);
		}

		private static void RemoveAllCheckmark()
		{
			Menu.SetChecked(kQaMenuItem, false);
			Menu.SetChecked(kLiveMenuItem, false);
			Menu.SetChecked(kDevMenuItem, false);
			Menu.SetChecked(kLocalMenuItem, false);
			Menu.SetChecked(kStageMenuItem, false);
		}

		private static void SetTo(string environment)
		{
			Menu.SetChecked(environment, true);

			switch (environment)
			{
				case kQaMenuItem:
					SetEditorEnvironment(ServerConfig.Environment.Qa);
					break;
				case kLiveMenuItem:
					SetEditorEnvironment(ServerConfig.Environment.Live);
					break;
				case kDevMenuItem:
					SetEditorEnvironment(ServerConfig.Environment.Dev);
					break;
				case kLocalMenuItem:
					SetEditorEnvironment(ServerConfig.Environment.Local);
					break;
				case kStageMenuItem:
					SetEditorEnvironment(ServerConfig.Environment.Stage);
					break;
			}
		}

		private static void SetEditorEnvironment(ServerConfig.Environment environement)
		{
			EditorPrefs.SetInt("RequestEnvironmentSetTo", (int)environement);
			Core.Debug.Log("Request", string.Format("Environment set to {0}", environement));
		}
	}
}
