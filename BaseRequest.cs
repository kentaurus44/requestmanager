﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Request
{
	public abstract class AttributeIn { }
	public abstract class AttributeOut { }

	public abstract class BaseRequest
	{
		public abstract string Route { get; }
		public abstract Dictionary<string, string> Headers { get; }
		public abstract string ObjectToJson();
		public abstract void JsonToObject(string json);
	}

	public abstract class BaseRequest<T, U> : BaseRequest where T : AttributeIn, new() where U : AttributeOut
	{
		protected T DataIn = new T();
		protected U DataOut;

		public override string ObjectToJson()
		{
			return JsonUtility.ToJson(DataIn);
		}

		public override void JsonToObject(string json)
		{
			DataOut = JsonUtility.FromJson<U>(json);
		}
	}
}