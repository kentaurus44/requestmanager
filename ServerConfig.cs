﻿using System.Collections.Generic;
using UnityEngine;
using System;

namespace Request
{
	[CreateAssetMenu(fileName = "ServerConfigs", menuName = "Config/ServerConfigs", order = 1)]
	public class ServerConfig : ScriptableObject
	{
		public enum Environment
		{
			Live = 0,
			Stage = 1,
			Dev = 2,
			Qa = 3,
			Local = 4
		}

		[Serializable]
		public class ServerURL
		{
			[SerializeField]
			public Environment Environment;
			[SerializeField]
			public string URL;
		}

		[SerializeField]
		private ServerURL[] _serverUrls = new ServerURL[0];

		public ServerURL[] ServerUrls { get { return _serverUrls; } }
	}
}
